#include<stdio.h>
#include<math.h>
struct points
{
    float x,y;
};
float distance(struct points a, struct points b)
{
    float res;
    res =  sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
    return res;
}
int main()
{
    struct points a,b;
    printf("Enter 1st coordinates : \n");
    scanf("%f%f",&a.x,&a.y);
    printf("Enter 2nd coordinates: \n");
    scanf("%f%f",&b.x,&b.y);
    printf("The Distance is : %f\n",distance(a,b));
}
